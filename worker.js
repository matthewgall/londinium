addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

async function getTubeStatus() {
  init = {
    headers: {
      'Accept': 'application/json'
    }
  }
  data = await fetch('https://api.tfl.gov.uk/Line/Mode/tube,dlr,overground/Status?detail=true', init)
  data = await data.json()
  
  output = []
  for (i=0; i < data.length; i++) {
    output.push({
      'name': data[i]['name'],
      'status': data[i]['lineStatuses'][0]['statusSeverityDescription']
    })
  }
  return output
}

async function getWeather() {
  let headers = {
    'Accept': 'application/json'
  }
  const init = {
    method: 'GET',
    headers: headers
  }
  data = await fetch('https://darksky.bots.noserverhe.re/location/51.5051,-0.0998', init)
  data = await data.json()
  return data
}

async function getRailStatus() {
  init = {
    headers: {
      'Accept': 'application/json'
    }
  }
  data = await fetch('https://api.tfl.gov.uk/line/mode/national-rail/status', init)
  data = await data.json()
  
  output = []
  for (i=0; i < data.length; i++) {
    output.push({
      'name': data[i]['name'],
      'status': data[i]['lineStatuses'][0]['statusSeverityDescription']
    })
  }
  return output
}

async function getRiverStatus() {
  init = {
    headers: {
      'Accept': 'application/json'
    }
  }
  data = await fetch('https://api.tfl.gov.uk/line/mode/river-bus/status', init)
  data = await data.json()
  
  output = []
  for (i=0; i < data.length; i++) {
    output.push({
      'name': data[i]['name'],
      'status': data[i]['lineStatuses'][0]['statusSeverityDescription']
    })
  }
  return output
}

async function getFreeRooms() {
  let content = `{
    "message": {
      "text": "london"
    }
  }`
  let headers = {
    'Content-Type': 'application/json'
  }
  const init = {
    method: 'POST',
    headers: headers,
    body: content
  }
  response = await fetch('https://robinrooms.bots.noserverhe.re/webhook/robinrooms', init)
  return response
}

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
  path = new URL(request.url).pathname;
  
  if (path == '/') {
    return new Response(`Direct access to this location is not permitted`, {
      headers: {
        'Content-Type': 'text/plain'
      }
    });
  }

  if (path.startsWith('/webhook/') && request.method == 'POST') {
    
    payload = await request.json()
    body = await payload

    // Now we split the message
    data = body['message']['text'].toLowerCase().replace(/ +(?= )/g, '').replace('@londinium', '').trim().split(' ')
    message = `Sorry, I didn't quite understand that.`

    if (data[0] == "tube") {
      tubestatus = await getTubeStatus()

      message = "Here is the current status of all London Underground lines:\n\n"
      for(j=0; j < tubestatus.length; j++) {
        message += `*${tubestatus[j]['name']}:* ${tubestatus[j]['status']}\n`
      }
    }

    if (data[0] == "rail") {
      tubestatus = await getRailStatus()

      message = "Here is the current status of all National Rail services:\n\n"
      for(j=0; j < tubestatus.length; j++) {
        message += `*${tubestatus[j]['name']}:* ${tubestatus[j]['status']}\n`
      }
    }

    if (isInArray(data[0], ['river', 'boat'])) {
      riverstatus = await getRiverStatus()

      message = "Here is the current status of all River Boat services:\n\n"
      for(let j=0; j < riverstatus.length; j++) {
        message += `*${riverstatus[j]['name']}:* ${riverstatus[j]['status']}\n`
      }
    }

    if (isInArray(data[0], ["rooms", "room"])) {
      rooms = getFreeRooms()
      return rooms
    }

    if (isInArray(data[0], ["weather", "forecast"])) {
      weather = await getWeather()
      message = `Here is the weather forecast for Southwark\n\n${weather['hourly']['summary']}\n\n_Data provided by darksky.net_`
    }

    if (isInArray(data[0], ['vote'])) {
      let voteData = await londinium.get('vote')
      if (voteData === null) {
        message = `There is currently no vote running at this time`
      }
      else {
        voteData = JSON.parse(voteData)
        message = `${voteData['question']}\n\n*Please select from one of the following options:*\n`
        for (let i=0; i < voteData['options'].length; i++) {
          message += `${voteData['options'][i]}\n`
        }
      }
    }

    // Now we see if there is a help document for this
    endpoint = `https://gitlab.com/matthewgall/londinium/raw/master/docs/${data[0]}.md`
    init = {
      headers: {
        'Accept': '*/*'
      }
    }
    data = await fetch(endpoint, init)
    if (data.status == 200) {
      message = await data.text()
    }
    
    body = {
      'text': message
    }
    return new Response(JSON.stringify(body), {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
  else {
    return new Response('Welcome to londinium', {
      headers: {
        'Content-Type': 'text/plain'
      }
    })
  }
}