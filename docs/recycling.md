Saving the environment in the London office is one of our goals. We have recycling locations for the following materials:

*Glass Bottles*: The recycling bin can be found under the TV in the kitchen on the 2nd Floor. This is *red*
*Batteries*: The recycling bin can be found on the 2nd shelf, near the safe on the 2nd Floor. This is *black*
*Paper:* The recycling bin can be found next to the photocopier on the 2nd Floor. This is *blue*
*Plastic, Cardboard and other recyclables*: Large plastic and cardboard can be left under the TV in the kitchen. Small bits of any other recyclable waste can be placed in the recycling bin under the sink in the kitchen in the cupboard to the left. This is *light blue*