I'm here to help with any enquiries you may have about the Cloudflare London office, or surrounding connectivity.
      
Right now, I support the following commands:
*help*: Displays this help text, obviously
*recycling*: I'll provide important information regarding recycling services in the office
*rail*: I'll provide service updates for all National Rail services
*rooms*: I'll provide a list of free rooms in the London office
*tube*: I'll provide service updates for all London Underground, and DLR services

_p.s: If you see something that doesn't look right, speak to station staff, or contact Robot Police on mgall@cloudflare.com. We'll sort it. See it, say it, sorted._