There are multiple locations for toilets within the building, these are as follows:

*1st Floor*:
On the left and right hand side of the kitchen in the shared area as you go through the double doors.
*2nd Floor*:
Male toilets are located closest to the double doors entering the floor. Female toilets are located on the left, a short walk past these. Disabled toilets are located inside the shower area, on the left hand side as you enter.
*3rd Floor*:
There are no toilets on this floor. Closest toilets are on the *2nd Floor* approximately 3 minutes walk out of the fire escape, via the balcony