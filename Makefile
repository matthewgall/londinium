NAME := googlechat-londinium

.PHONY: deploy
deploy:
	curl -X PUT "https://api.cloudflare.com/client/v4/accounts/${CLOUDFLARE_ORG_ID}/workers/scripts/${NAME}" -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" -H "X-Auth-Key:${CLOUDFLARE_KEY}" -F 'script=@worker.js;type=application/javascript' -F 'metadata={"body_part": "script", "bindings": [{"name": "${CLOUDFLARE_KV}", "type": "kv_namespace", "namespace_id": "${CLOUDFLARE_KV_NAMESPACEID}"}]};type=application/json'

.PHONY: deploy-vote
deploy-vote:
	curl "https://api.cloudflare.com/client/v4/accounts/${CLOUDFLARE_ORG_ID}/storage/kv/namespaces/${CLOUDFLARE_KV_NAMESPACEID}/values/vote" -X PUT -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" -H "X-Auth-Key:${CLOUDFLARE_KEY}" --data @vote.json

.PHONY: deploy-docs
deploy-docs:
	aws s3 --endpoint-url=https://s3.wasabisys.com sync --acl public-read ./docs s3://matthewgall/londinium